import React from "react";
import "./App.css";

function App() {
  const [src, setSrc] = React.useState("");
  const inputRef = React.useRef();
  const canvasRef = React.useRef();

  const handleFileChange = (e) => {
    const file = inputRef.current.files[0];
    console.log(file);
    const reader = new FileReader();
    const image = new Image();

    image.onload = function () {
      console.log("!");
      console.log(canvasRef.current);
      let canvasCtx = canvasRef.current.getContext("2d");
      console.log(canvasCtx);

      canvasRef.current.width = image.width;
      canvasRef.current.height = image.height;
      canvasCtx.filter = "grayscale(1)";
      canvasCtx.drawImage(image, 0, 0, image.width, image.height);
    };

    reader.onloadend = function (e) {
      setSrc(reader.result);
      console.log(reader.result);
      image.src = reader.result;
    };

    reader.readAsDataURL(file);
  };

  const saveImage = () => {
    var link = document.createElement("a");
    link.download = "download.png";
    link.href = canvasRef.current.toDataURL();
    link.click();
  };

  return (
    <div className="App">
      <input
        ref={inputRef}
        type="file"
        accept="image/*"
        onChange={handleFileChange}
      />
      <button type="button" onClick={saveImage}>
        save
      </button>
      <img style={{ width: 100 }} src={src} alt="" />
      <canvas ref={canvasRef} id="output"></canvas>
    </div>
  );
}

export default App;
